sap.ui.define([
	"zmhaui5aprovact/controller/BaseController",
	'sap/ui/model/Filter'
], function(BaseController, Filter) {
	"use strict";

	return BaseController.extend("zmhaui5aprovact.controller.ListaRegras", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf zmhaui5aprovact.controller.view.ListaRegras
		 */
		onInit: function() {
			var oRouter;
			oRouter = this.getRouter();
			oRouter.getRoute("t_lista").attachPatternMatched(this._onPostMatched, this);

		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf zmhaui5aprovact.controller.view.ListaRegras
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf zmhaui5aprovact.controller.view.ListaRegras
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf zmhaui5aprovact.controller.view.ListaRegras
		 */
		//	onExit: function() {
		//
		//	}

		_onPostMatched: function(oEvent) {
			var oView = this.getView();
			var oTable = oView.byId("tableLista");
			var oBinding = oTable.getBinding("items");
			var oFilter = {};
			var aFilter = [];

			oFilter = new Filter("bukrs", sap.ui.model.FilterOperator.EQ, oEvent.getParameter("arguments").bukrs);
			aFilter.push(oFilter);

			oFilter = new Filter("werks", sap.ui.model.FilterOperator.EQ, oEvent.getParameter("arguments").werks);
			aFilter.push(oFilter);

            // Campo não será mais utilizado - 07/05/2018
			//oFilter = new Filter("tp_negocio", sap.ui.model.FilterOperator.EQ, oEvent.getParameter("arguments").tp_negocio);
			//aFilter.push(oFilter);

			oFilter = new Filter("id_tpdoc_wf", sap.ui.model.FilterOperator.EQ, oEvent.getParameter("arguments").id_tpdoc_wf);
			aFilter.push(oFilter);

			// oFilter = new Filter("kostl", sap.ui.model.FilterOperator.EQ, oEvent.getParameter("arguments").kostl);
			// aFilter.push(oFilter);

			// oFilter = new Filter("ldgrp", sap.ui.model.FilterOperator.EQ, oEvent.getParameter("arguments").ldgrp);
			// aFilter.push(oFilter);

			// oFilter = new Filter("ekgrp", sap.ui.model.FilterOperator.EQ, oEvent.getParameter("arguments").ekgrp);
			// aFilter.push(oFilter);

			// oFilter = new Filter("vkorg", sap.ui.model.FilterOperator.EQ, oEvent.getParameter("arguments").vkorg);
			// aFilter.push(oFilter);

			// oFilter = new Filter("spart", sap.ui.model.FilterOperator.EQ, oEvent.getParameter("arguments").spart);
			// aFilter.push(oFilter);

			// oFilter = new Filter("vtweg", sap.ui.model.FilterOperator.EQ, oEvent.getParameter("arguments").vtweg);
			// aFilter.push(oFilter);

			oBinding.filter(aFilter);

		},
		// override the parent's onNavBack (inherited from BaseController)
		onNavBack: function(oEvent) {
			this.myNavBack("t_main");
		},
		
		onItemPress: function(oEvent){
			var oBinding = oEvent.getSource().getBindingContext();

			this.getRouter().navTo("t_aprov",{
					id_valr_centro: oBinding.getProperty("id_valr_centro"),
					//id_regra_wf: oBinding.getProperty("id_regra_wf"),
					multi_aprovador: oBinding.getProperty("multi_aprovador")
				});

		}

	});

});