sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"zmhaui5aprovact/model/models",
	"sap/ui/model/json/JSONModel"
], function(UIComponent, Device, models, JSONModel) {
	"use strict";

	return UIComponent.extend("zmhaui5aprovact.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set the device model
			this.setModel(models.createDeviceModel(), "device");
			
			var oModelBusyView = new JSONModel({busy: false });
			this.setModel(oModelBusyView, "busyAppView");
			
			var oModelGlobal = new JSONModel(
				 {
				 	fromAprovador: false,
				 	kostl: "",
                    rldnr: "",
                    ekgrp: "",
                    vkorg: "",
                    spart: "",
                    vtweg: "",
                    blart: ""
				 });
				 
			this.setModel(oModelGlobal, "global");
	
     		// create the views based on the url/hash
			this.getRouter().initialize();		
		}
	});
});