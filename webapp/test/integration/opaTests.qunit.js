/* global QUnit */

sap.ui.require(["zmhaui5aprovact/test/integration/AllJourneys"
], function () {
	QUnit.config.autostart = false;
	QUnit.start();
});
