sap.ui.define([
	"zmhaui5aprovact/controller/BaseController",
	'sap/ui/model/json/JSONModel',
	'sap/ui/model/Filter',
	"sap/m/MessageBox"
], function(BaseController, JSONModel, Filter, MessageBox) {
	"use strict";

	return BaseController.extend("zmhaui5aprovact.controller.Aprovador", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf zmhaui5aprovact.controller.view.Aprovador
		 */
		onInit: function() {

			var oViewModel = new JSONModel();
			this.setModel(oViewModel, "displayView");

			/*			var aAprov = [];
						var teste = {
							vigente: 'ABC',
							novo: 'DFG'
						};
						aAprov.push(teste);
						oViewModel.setProperty("/aAprovador", aAprov);*/

			var oRouter;
			oRouter = this.getRouter();
			oRouter.getRoute("t_aprov").attachPatternMatched(this._onPostMatched, this);

		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf zmhaui5aprovact.controller.view.Aprovador
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf zmhaui5aprovact.controller.view.Aprovador
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf zmhaui5aprovact.controller.view.Aprovador
		 */
		//	onExit: function() {
		//
		//	}
		_onPostMatched: function(oEvent) {

			var oModelView = this.getModel("displayView");
			oModelView.setProperty("/id_valr_centro", oEvent.getParameter("arguments").id_valr_centro);
			//oModelView.setProperty("/id_regra_wf", oEvent.getParameter("arguments").id_regra_wf);
			oModelView.setProperty("/multi_aprovador", oEvent.getParameter("arguments").multi_aprovador);

			this.buscaAprovadores(oEvent.getParameter("arguments").id_valr_centro);

		},

		buscaAprovadores: function(idValrCentro) {
			var oGWModel = this.getView().getModel();
			var url = "";
			var aFilter = [];
			var oFilter = {};

			url = "/ZMHACDS_APROV('" + idValrCentro + "')/to_MultiAprv";

			oFilter = new Filter("kostl", sap.ui.model.FilterOperator.EQ, this.getOwnerComponent().getModel("global").getProperty(
				"/kostl"));
			aFilter.push(oFilter);

			oFilter = new Filter("ldgrp", sap.ui.model.FilterOperator.EQ, this.getOwnerComponent().getModel("global").getProperty(
				"/ldgrp"));
			aFilter.push(oFilter);

			oFilter = new Filter("ekgrp", sap.ui.model.FilterOperator.EQ, this.getOwnerComponent().getModel("global").getProperty(
				"/ekgrp"));
			aFilter.push(oFilter);

			oFilter = new Filter("vkorg", sap.ui.model.FilterOperator.EQ, this.getOwnerComponent().getModel("global").getProperty(
				"/vkorg"));
			aFilter.push(oFilter);

			oFilter = new Filter("spart", sap.ui.model.FilterOperator.EQ, this.getOwnerComponent().getModel("global").getProperty(
				"/spart"));
			aFilter.push(oFilter);

			oFilter = new Filter("vtweg", sap.ui.model.FilterOperator.EQ, this.getOwnerComponent().getModel("global").getProperty(
				"/vtweg"));
			aFilter.push(oFilter);
			
			oFilter = new Filter("blart", sap.ui.model.FilterOperator.EQ, this.getOwnerComponent().getModel("global").getProperty(
				"/blart"));
			aFilter.push(oFilter);			

			this.getOwnerComponent().getModel("busyAppView").setProperty("/busy", true);
			oGWModel.setUseBatch(false);
			oGWModel.read(url, // Entity Set
				{
					context: null,
					async: false,
					filters: aFilter,
					sorters: null,
					success: this._onReadSuccessMultiAprov.bind(this),
					error: this._onReadErrorMultiAprov.bind(this)

				});
		},

		_onReadSuccessMultiAprov: function(oData, oResponse) {
			var oViewModel = this.getView().getModel("displayView");
			var aAprovador = [];

			for (var i = 0; i < oData.results.length; i++) {
				var oAprovador = {};

				oAprovador.id_multi_aprov = oData.results[i].id_multi_aprov;
				oAprovador.id_regra_wf = oData.results[i].id_regra_wf;
				oAprovador.vigente = oData.results[i].bname;
				oAprovador.nome_vigente = oData.results[i].nome_vigente;
				oAprovador.novo = oData.results[i].bname_new;
				oAprovador.nome_novo = oData.results[i].nome_novo;
				oAprovador.delflag = false;

				aAprovador.push(oAprovador);
				
				if (oAprovador.id_regra_wf !== "") {

					oViewModel.setProperty("/id_regra_wf", oAprovador.id_regra_wf);

				}
			}
			oViewModel.setProperty("/aAprovador", aAprovador);
			this.getOwnerComponent().getModel("busyAppView").setProperty("/busy", false);

		},

		_onReadErrorMultiAprov: function(oError) {

			this.getOwnerComponent().getModel("busyAppView").setProperty("/busy", false);
		},

		onNavBack: function(oEvent) {
			this.myNavBack("t_lista");
		},

		handleValueHelp: function(oEvent) {
			var sInputValue = oEvent.getSource().getValue();
			var sView = "";
			var sField = "";
			this.inputId = oEvent.getSource().getId();
			if (this.inputId.indexOf("tbInputUsrNovo") !== -1) {
				sView = "zmhaui5aprovact.view.UserSH";
				sField = "bname";
			}

			if (this._valueHelpDialog) {
				this._valueHelpDialog.destroy();
			}

			this._valueHelpDialog = sap.ui.xmlfragment(
				sView,
				this
			);
			this.getView().addDependent(this._valueHelpDialog);

			// create a filter for the binding
			this._valueHelpDialog.getBinding("items").filter([new Filter(
				sField,
				sap.ui.model.FilterOperator.Contains, sInputValue
			)]);

			if (oEvent.getSource() instanceof sap.m.MultiInput) {
				this._valueHelpDialog.setMultiSelect(true);
			}
			// open value help dialog filtered by the input value
			this._valueHelpDialog.open(sInputValue);
		},

		_handleValueHelpSearch: function(oEvent) {
			var sValue = oEvent.getParameter("value").toString();
			var oFilter = {};

			if (this.inputId.indexOf("tbInputUsrNovo") !== -1) {
				oFilter = new Filter("bname", sap.ui.model.FilterOperator.Contains, sValue);
				oFilter = new Filter("descr", sap.ui.model.FilterOperator.Contains, sValue);
			}

			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter([oFilter]);
		},

		_handleValueHelpClose: function(oEvent) {
			var oModelView = this.getModel("displayView");
			var inputField = this.getView().byId(this.inputId);

			if (inputField instanceof sap.m.Input) {
				var oSelectedItem = oEvent.getParameter("selectedItem");

				if (oSelectedItem) {
					inputField.setValue(oSelectedItem.getCells()[0].getText());

					if (this.inputId.indexOf("tbInputUsrNovo") !== -1) {
						var oIfBind = inputField.getBinding("value");
						var oIfCtx = oIfBind.getContext();
						var sPath = oIfCtx.getPath();
						var sPathDesc = "";

						sPathDesc = sPath + "/novo";
						oModelView.setProperty(sPathDesc, oSelectedItem.getCells()[0].getText());

						sPathDesc = sPath + "/nome_novo";
						oModelView.setProperty(sPathDesc, oSelectedItem.getCells()[1].getText());
					}
				}
			}
			oEvent.getSource().getBinding("items").filter([]);
		},

		onLiveChange: function(oEvent) {
			var oModelView = this.getModel("displayView");
			var sValue = oEvent.getParameter("value");
			var sId = oEvent.getParameter("id");

			if (typeof sValue === "undefined" || sValue === "") {
				if (sId.indexOf("tbInputUsrNovo") !== -1) {
					var oSrc = oEvent.getSource();
					var oSrcBind = oSrc.getBinding("value");
					var oSrcCtx = oSrcBind.getContext();
					var sPath = oSrcCtx.getPath();
					var sPathDesc = "";

					sPathDesc = sPath + "/novo";
					oModelView.setProperty(sPathDesc, "");

					sPathDesc = sPath + "/nome_novo";
					oModelView.setProperty(sPathDesc, "");
				}
			}

		},

		onAddRowPress: function(oEvent) {
			var oModelView = this.getView().getModel("displayView");
			var oDataView = oModelView.getData();

			if (!oDataView.aAprovadores) {
				oDataView.aAprovadores = [];
			}

			var oAprovador = {};
			oAprovador.id_multi_aprov = "";
			oAprovador.id_regra_wf = "";
			oAprovador.vigente = "";
			oAprovador.nome_vigente = "";
			oAprovador.novo = "";
			oAprovador.nome_novo = "";
			oAprovador.delflag = false;

			oDataView.aAprovador.push(oAprovador);

			oModelView.refresh();

		},

		onRowDeletePress: function(oEvent) {

			var oListItem = oEvent.getParameter("listItem");
			var oModel = this.getView().getModel("displayView");
			var oData = oModel.getData();
			var i = parseInt(oListItem.getBindingContextPath().replace(/\D/g, ""), 10);

			if (oData.aAprovador[i].id_multi_aprov !== "") {
				oData.aAprovador[i].delflag = true;
			} else {
				oData.aAprovador.splice(i, 1);
			}
			oModel.refresh();

		},

		onExecute: function(oEvent) {

			var oView = this.getView(),
				oModelView = oView.getModel("displayView"),
				oDataView = oModelView.getData(),
				bInvalidar = false;

			for (var i = 0; i < oDataView.aAprovador.length; i++) {

				if (oDataView.aAprovador[i].delflag === true) {
					bInvalidar = true;
					break;
				}

			}

			if (bInvalidar) {
				MessageBox.warning(
					this.getResourceBundle().getText("aprovador_m01"), {
						actions: [sap.m.MessageBox.Action.OK, sap.m.MessageBox.Action.CANCEL],
						onClose: function(sAction) {

							if (sAction === "OK") {
								this.callAprovadorCD();
							}

						}.bind(this)
					}
				);
			} else {
				this.callAprovadorCD();
			}
		},

		callAprovadorCD: function() {

			var oView = this.getView(),
				oModelGW = oView.getModel("aprovador"),
				oModelView = oView.getModel("displayView"),
				oDataView = oModelView.getData(),
				sUrl = "/AprovarCDSet",
				oData = {};

			//Preenche o header do Create Deep
			oData.idValrCentro = oDataView.id_valr_centro;
			//oData.idRegraWf = oDataView.id_regra_wf;
			oData.multiAprovador = (oDataView.multi_aprovador === "true");
			oData.kostl = this.getOwnerComponent().getModel("global").getProperty("/kostl");
			oData.ldgrp = this.getOwnerComponent().getModel("global").getProperty("/ldgrp");
			oData.ekgrp = this.getOwnerComponent().getModel("global").getProperty("/ekgrp");
			oData.vkorg = this.getOwnerComponent().getModel("global").getProperty("/vkorg");
			oData.spart = this.getOwnerComponent().getModel("global").getProperty("/spart");
			oData.vtweg = this.getOwnerComponent().getModel("global").getProperty("/vtweg");
			oData.blart = this.getOwnerComponent().getModel("global").getProperty("/blart");

			//Preenche os itens do Create Deep
			oData.ToAprovadores = [];

			var aAprovador = oDataView.aAprovador;
			var oAprovador = {};
			for (var i = 0; i < aAprovador.length; i++) {
				oAprovador = {};

				oAprovador.idValrCentro = oData.idValrCentro;
				oAprovador.idRegraWf = aAprovador[i].id_regra_wf;
				oAprovador.idMultiAprov = aAprovador[i].id_multi_aprov;
				oAprovador.bname = aAprovador[i].vigente;
				oAprovador.bnameNew = aAprovador[i].novo;
				oAprovador.delflag = aAprovador[i].delflag;

				oData.ToAprovadores.push(oAprovador);
			}

			this.getOwnerComponent().getModel("busyAppView").setProperty("/busy", true);

			//Executa o Create Deep
			oModelGW.create(sUrl, // Entity Set
				oData, {
					context: null,
					success: function(oData, oResponse) {
						this.getOwnerComponent().getModel("busyAppView").setProperty("/busy", false);

						MessageBox.success(JSON.parse(oResponse.headers["sap-message"]).message, {
							onClose: function() {
								//this.getOwnerComponent().getModel("global").setProperty("/fromAprovador", true);
								//this.getRouter().navTo("t_main");
								this.myNavBack("t_lista");
							}.bind(this)
						});

					}.bind(this), // callback function for success

					error: function(oError) {
						this.getOwnerComponent().getModel("busyAppView").setProperty("/busy", false);

						MessageBox.error(JSON.parse(oError.responseText).error.message.value);

					}.bind(this)

				}); // callback function for error 				

		}

	});

});