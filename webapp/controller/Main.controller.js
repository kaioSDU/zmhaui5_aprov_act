sap.ui.define([
	"zmhaui5aprovact/controller/BaseController",
	'sap/ui/core/Fragment',
	'sap/ui/model/Filter',
	'sap/ui/model/json/JSONModel',
	"sap/m/MessageBox"
], function(BaseController, Fragment, Filter, JSONModel, MessageBox) {
	"use strict";

	return BaseController.extend("zmhaui5aprovact.controller.Main", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf zmhaui5aprovact.view.Main
		 */
		onInit: function() {
			var oViewModel = new JSONModel();
			this.setModel(oViewModel, "displayView");
			//          Campos Fixos			
			oViewModel.setProperty("/bukrs", "");
			oViewModel.setProperty("/werks", "");
			oViewModel.setProperty("/tp_negocio", "");
			oViewModel.setProperty("/id_tpdoc_wf", "");
			//          Campos dinâmicos			
			oViewModel.setProperty("/kostl", "");
			oViewModel.setProperty("/ldgrp", "");
			oViewModel.setProperty("/ekgrp", "");
			oViewModel.setProperty("/vkorg", "");
			oViewModel.setProperty("/spart", "");
			oViewModel.setProperty("/vtweg", "");
			oViewModel.setProperty("/blart", "");
			//          Campos flags controle visibilidade			
			oViewModel.setProperty("/e_cc", false);
			oViewModel.setProperty("/e_gc", false);
			oViewModel.setProperty("/e_ov", false);
			oViewModel.setProperty("/e_ledger", false);
			oViewModel.setProperty("/e_blart", false);
			oViewModel.attachPropertyChange(this._onPropertyChange, this);

			var oRouter;
			oRouter = this.getRouter();
			oRouter.getRoute("t_main").attachPatternMatched(this._onPostMatched, this);
			
			var me = this;
			var oCheckUser = this.getOwnerComponent().getModel("check_user");
			// oCheckUser.read("/ZMHACDS_CC_CHECK_USER", {
			// 	success: function(data) { me.setModel(new JSONModel(data), "res_check_model"); },
			// 	error: function(data) { } 
			// }, null, false);

		},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf zmhaui5aprovact.view.Main
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf zmhaui5aprovact.view.Main
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf zmhaui5aprovact.view.Main
		 */
		//	onExit: function() {
		//
		//	}

		_onPostMatched: function(oEvent) {
			//			var oViewModel = this.getView().getModel("displayView");
			/*
						if (this.getOwnerComponent().getModel("global").getProperty("/fromAprovador")) {

							oViewModel.setProperty("/bukrs", "");
							oViewModel.setProperty("/werks", "");
							oViewModel.setProperty("/tp_negocio", "");
							oViewModel.setProperty("/id_tpdoc_wf", "");
							//          Campos dinâmicos			
							oViewModel.setProperty("/kostl", "");
							oViewModel.setProperty("/ldgrp", "");
							oViewModel.setProperty("/ekgrp", "");
							oViewModel.setProperty("/vkorg", "");
							oViewModel.setProperty("/spart", "");
							oViewModel.setProperty("/vtweg", "");
							//          Campos flags controle visibilidade			
							oViewModel.setProperty("/e_cc", false);
							oViewModel.setProperty("/e_gc", false);
							oViewModel.setProperty("/e_ov", false);
							oViewModel.setProperty("/e_ledger", false);

						}

						this.getOwnerComponent().getModel("global").setProperty("/fromAprovador", false);
			*/
			this.getOwnerComponent().getModel("global").setProperty("/kostl", "");
			this.getOwnerComponent().getModel("global").setProperty("/ldgrp", "");
			this.getOwnerComponent().getModel("global").setProperty("/ekgrp", "");
			this.getOwnerComponent().getModel("global").setProperty("/vkorg", "");
			this.getOwnerComponent().getModel("global").setProperty("/spart", "");
			this.getOwnerComponent().getModel("global").setProperty("/vtweg", "");
			this.getOwnerComponent().getModel("global").setProperty("/blart", "");

		},

		_onPropertyChange: function(oEvent) {
			var oGWModel = this.getView().getModel();
			var oViewModel = this.getView().getModel("displayView");
			var url = "";

			if (oEvent.getParameters().path === "/id_tpdoc_wf" ||
				oEvent.getParameters().path === "id_tpdoc_wf") {

				oViewModel.setProperty("/kostl", "");
				oViewModel.setProperty("/ldgrp", "");
				oViewModel.setProperty("/ekgrp", "");
				oViewModel.setProperty("/vkorg", "");
				oViewModel.setProperty("/spart", "");
				oViewModel.setProperty("/vtweg", "");
				oViewModel.setProperty("/blart", "");

				oViewModel.setProperty("/e_cc", false);
				oViewModel.setProperty("/e_gc", false);
				oViewModel.setProperty("/e_ov", false);
				oViewModel.setProperty("/e_ledger", false);
				oViewModel.setProperty("/e_blart", false);

				//url = "/ZISH_TPDOC_APROV('" + oViewModel.getProperty("/id_tpdoc_wf") + "')";
				url = "/ZMHAT_TPDOC_WF('" + oViewModel.getProperty("/id_tpdoc_wf") + "')";

				this.getOwnerComponent().getModel("busyAppView").setProperty("/busy", true);
				oGWModel.setUseBatch(false);
				oGWModel.read(url, // Entity Set
					{
						context: null,
						async: false,
						//filters: aFilter,
						sorters: null,
						success: this._onReadSuccessTpDoc.bind(this),
						error: this._onReadErrorTpDoc.bind(this)

					});
			}

		},

		_onReadSuccessTpDoc: function(oData, oResponse) {
			var oViewModel = this.getView().getModel("displayView");
			oViewModel.setProperty("/e_cc", oData.E_CC);
			oViewModel.setProperty("/e_gc", oData.E_GC);
			oViewModel.setProperty("/e_ov", oData.E_OV);
			oViewModel.setProperty("/e_ledger", oData.E_LEDGER);
			oViewModel.setProperty("/e_blart", oData.E_BLART);
			this.getOwnerComponent().getModel("busyAppView").setProperty("/busy", false);
		},

		_onReadErrorTpDoc: function(oError) {
			var oViewModel = this.getView().getModel("displayView");
			oViewModel.setProperty("/e_cc", false);
			oViewModel.setProperty("/e_gc", false);
			oViewModel.setProperty("/e_ov", false);
			oViewModel.setProperty("/e_ledger", false);
			oViewModel.setProperty("/e_blart", false);
			this.getOwnerComponent().getModel("busyAppView").setProperty("/busy", false);
		},

		handleValueHelp: function(oEvent) {
			var oViewModel = this.getView().getModel("displayView");
			var sInputValue = oEvent.getSource().getValue();
			var sView = "";
			var sField = "";
			var bHasFilterAdd = false;
			var oFilter = {};
			var aFilter = [];
			var addDefaultFilter = true;

			this.inputId = oEvent.getSource().getId();

			if (this.inputId.indexOf("inputEmpresa") !== -1) {
				sView = "zmhaui5aprovact.view.EmpresaSH";
				sField = "bukrs";
			} else if (this.inputId.indexOf("inputCentro") !== -1) {
				sView = "zmhaui5aprovact.view.CentroSH";
				sField = "werks";

				oFilter = new Filter("bukrs", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/bukrs"));
				aFilter.push(oFilter);

			} else if (this.inputId.indexOf("inputTpNegocio") !== -1) {
				sView = "zmhaui5aprovact.view.TpNegocioSH";
				sField = "tp_negocio";

				oFilter = new Filter("bukrs", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/bukrs"));
				aFilter.push(oFilter);

				oFilter = new Filter("werks", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/werks"));
				aFilter.push(oFilter);

			} else if (this.inputId.indexOf("inputTpDoc") !== -1) {
				sView = "zmhaui5aprovact.view.TpDocSH";
				sField = "id_tpdoc_wf";

				oFilter = new Filter("bukrs", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/bukrs"));
				aFilter.push(oFilter);

				oFilter = new Filter("werks", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/werks"));
				aFilter.push(oFilter);

				//	oFilter = new Filter("tp_negocio", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/tp_negocio"));
				//	aFilter.push(oFilter);				

			} else if (this.inputId.indexOf("inputCC") !== -1) {
				var werks = oViewModel.getProperty("/werks");
				var ck = this.getModel("res_check_model");
				
				var f1 = new Filter("kostl", sap.ui.model.FilterOperator.Contains, sInputValue);
				var f2 = new Filter("descr", sap.ui.model.FilterOperator.Contains, sInputValue);
				var f3 = new Filter("kostl", sap.ui.model.FilterOperator.StartsWith, werks);
				
				var fOr = new Filter([f1, f2], false); // filtro tipo Or
				var fFinal = null;
				
				if(ck && ck.oData && ck.oData.results && ck.oData.results.length >= 1) {
					fFinal = fOr;
				} else {
					fFinal = new Filter([f3, fOr], true); // filtro tipo And
				}
				
				aFilter.push(fFinal);
				
				addDefaultFilter = false;
				
				sView = "zmhaui5aprovact.view.CCSH";
				sField = "kostl";
			} else if (this.inputId.indexOf("inputLedger") !== -1) {
				sView = "zmhaui5aprovact.view.LedgerSH";
				sField = "ldgrp";
				
				oFilter = new Filter("id_tpdoc_wf", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/id_tpdoc_wf"));
				aFilter.push(oFilter);				
			} else if (this.inputId.indexOf("inputGC") !== -1) {
				sView = "zmhaui5aprovact.view.GCSH";
				sField = "ekgrp";
			} else if (this.inputId.indexOf("inputOV") !== -1) {
				sView = "zmhaui5aprovact.view.OVSH";
				sField = "vkorg";
			} else if (this.inputId.indexOf("inputSetor") !== -1) {
				sView = "zmhaui5aprovact.view.SetorSH";
				sField = "spart";
			} else if (this.inputId.indexOf("inputCanal") !== -1) {
				sView = "zmhaui5aprovact.view.CanalSH";
				sField = "vtweg";
			} else if (this.inputId.indexOf("inputBlart") !== -1) {
				sView = "zmhaui5aprovact.view.BlartSH";
				sField = "blart";
				
				oFilter = new Filter("id_tpdoc_wf", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/id_tpdoc_wf"));
				aFilter.push(oFilter);				
			}

			if (this._valueHelpDialog) {
				this._valueHelpDialog.destroy();
			}

			this._valueHelpDialog = sap.ui.xmlfragment(
				sView,
				this
			);
			this.getView().addDependent(this._valueHelpDialog);

			if(addDefaultFilter) {
				oFilter = new Filter(sField, sap.ui.model.FilterOperator.Contains, sInputValue);
				aFilter.push(oFilter);
			}

			if (bHasFilterAdd) {
				oFilter = new Filter("bukrs", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/bukrs"));
				aFilter.push(oFilter);

				oFilter = new Filter("werks", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/werks"));
				aFilter.push(oFilter);

				//	oFilter = new Filter("tp_negocio", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/tp_negocio"));
				//	aFilter.push(oFilter);

				oFilter = new Filter("id_tpdoc_wf", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/id_tpdoc_wf"));
				aFilter.push(oFilter);
			}

			// create a filter for the binding
			this._valueHelpDialog.getBinding("items").filter(aFilter);

			// open value help dialog filtered by the input value
			this._valueHelpDialog.open(sInputValue);
		},

		_handleValueHelpSearch: function(oEvent) {
			var sValue = oEvent.getParameter("value").toString();
			var oViewModel = this.getView().getModel("displayView");
			var oFilter = {};
			var aFilter = [];
			var bHasFilterAdd = false;

			if (this.inputId.indexOf("inputEmpresa") !== -1) {
				oFilter = new Filter("bukrs", sap.ui.model.FilterOperator.Contains, sValue);
				aFilter.push(oFilter);
			} else if (this.inputId.indexOf("inputCentro") !== -1) {

				oFilter = new Filter("bukrs", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/bukrs"));
				aFilter.push(oFilter);

				oFilter = new Filter("werks", sap.ui.model.FilterOperator.Contains, sValue);
				aFilter.push(oFilter);

			} else if (this.inputId.indexOf("inputTpNegocio") !== -1) {

				oFilter = new Filter("bukrs", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/bukrs"));
				aFilter.push(oFilter);

				oFilter = new Filter("werks", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/werks"));
				aFilter.push(oFilter);

				oFilter = new Filter("tp_negocio", sap.ui.model.FilterOperator.Contains, sValue);
				aFilter.push(oFilter);

			} else if (this.inputId.indexOf("inputTpDoc") !== -1) {

				oFilter = new Filter("bukrs", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/bukrs"));
				aFilter.push(oFilter);

				oFilter = new Filter("werks", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/werks"));
				aFilter.push(oFilter);

				//	oFilter = new Filter("tp_negocio", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/tp_negocio"));
				//	aFilter.push(oFilter); 

				oFilter = new Filter("id_tpdoc_wf", sap.ui.model.FilterOperator.Contains, sValue);
				aFilter.push(oFilter);

			} else if (this.inputId.indexOf("inputCC") !== -1) {
				var werks = oViewModel.getProperty("/werks");
				
				var f1 = new Filter("kostl", sap.ui.model.FilterOperator.Contains, sValue);
				var f2 = new Filter("descr", sap.ui.model.FilterOperator.Contains, sValue);
				var f3 = new Filter("kostl", sap.ui.model.FilterOperator.StartsWith, werks);
				
				var fOr = new Filter([f1, f2], false);
				var fAnd = new Filter([f3, fOr], true);
				
				aFilter.push(fAnd);
			} else if (this.inputId.indexOf("inputLedger") !== -1) {
				oFilter = new Filter("ldgrp", sap.ui.model.FilterOperator.Contains, sValue);
				aFilter.push(oFilter);

				oFilter = new Filter("id_tpdoc_wf", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/id_tpdoc_wf"));
				aFilter.push(oFilter);
			} else if (this.inputId.indexOf("inputGC") !== -1) {
				oFilter = new Filter("ekgrp", sap.ui.model.FilterOperator.Contains, sValue);
				aFilter.push(oFilter);
			} else if (this.inputId.indexOf("inputOV") !== -1) {
				oFilter = new Filter("vkorg", sap.ui.model.FilterOperator.Contains, sValue);
				aFilter.push(oFilter);
			} else if (this.inputId.indexOf("inputSetor") !== -1) {
				oFilter = new Filter("spart", sap.ui.model.FilterOperator.Contains, sValue);
				aFilter.push(oFilter);
			} else if (this.inputId.indexOf("inputCanal") !== -1) {
				oFilter = new Filter("vtweg", sap.ui.model.FilterOperator.Contains, sValue);
				aFilter.push(oFilter);
			} else if (this.inputId.indexOf("inputBlart") !== -1) {
				oFilter = new Filter("blart", sap.ui.model.FilterOperator.Contains, sValue);
				aFilter.push(oFilter);

				oFilter = new Filter("id_tpdoc_wf", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/id_tpdoc_wf"));
				aFilter.push(oFilter);
			}

			if (bHasFilterAdd) {
				oFilter = new Filter("bukrs", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/bukrs"));
				aFilter.push(oFilter);

				oFilter = new Filter("werks", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/werks"));
				aFilter.push(oFilter);

				//	oFilter = new Filter("tp_negocio", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/tp_negocio"));
				//	aFilter.push(oFilter);

				oFilter = new Filter("id_tpdoc_wf", sap.ui.model.FilterOperator.Contains, oViewModel.getProperty("/id_tpdoc_wf"));
				aFilter.push(oFilter);
			}

			var oBinding = oEvent.getSource().getBinding("items");
			oBinding.filter(aFilter);
		},

		_handleValueHelpClose: function(oEvent) {
			var inputField = this.getView().byId(this.inputId);

			if (inputField instanceof sap.m.Input) {
				var oSelectedItem = oEvent.getParameter("selectedItem");
				if (oSelectedItem) {
					var sText = oSelectedItem.getCells()[0].getText();
					inputField.setValue(sText);
					if (this.inputId.indexOf("inputEmpresa") !== -1) {
						this.getModel("displayView").setProperty("/bukrs", sText);
					} else if (this.inputId.indexOf("inputCentro") !== -1) {
						this.getModel("displayView").setProperty("/werks", sText);
					} else if (this.inputId.indexOf("inputTpNegocio") !== -1) {
						this.getModel("displayView").setProperty("/tp_negocio", sText);
					} else if (this.inputId.indexOf("inputTpDoc") !== -1) {
						this.getModel("displayView").setProperty("/id_tpdoc_wf", sText);
					} else if (this.inputId.indexOf("inputCC") !== -1) {
						this.getModel("displayView").setProperty("/kostl", sText);
					} else if (this.inputId.indexOf("inputLedger") !== -1) {
						this.getModel("displayView").setProperty("/ldgrp", sText);
					} else if (this.inputId.indexOf("inputGC") !== -1) {
						this.getModel("displayView").setProperty("/ekgrp", sText);
					} else if (this.inputId.indexOf("inputOV") !== -1) {
						this.getModel("displayView").setProperty("/vkorg", sText);
					} else if (this.inputId.indexOf("inputSetor") !== -1) {
						this.getModel("displayView").setProperty("/spart", sText);
					} else if (this.inputId.indexOf("inputCanal") !== -1) {
						this.getModel("displayView").setProperty("/vtweg", sText);
					} else if (this.inputId.indexOf("inputBlart") !== -1) {
						this.getModel("displayView").setProperty("/blart", sText);
					}

				}
			}
			oEvent.getSource().getBinding("items").filter([]);
		},

		onExecutar: function(oEvent) {
			var oView = this.getView();
			var oViewModel = this.getView().getModel("displayView");
			var oGWModel = oView.getModel("aprovador");
			var sUrl = "";
			//var oDevice = this.getView().getModel("device");

			var aInputs = [
				oView.byId("inputEmpresa"),
				oView.byId("inputCentro"),
				//	oView.byId("inputTpNegocio"),
				oView.byId("inputTpDoc")
			];

			if (oViewModel.getProperty("/e_cc")) {
				aInputs.push(oView.byId("inputCC"));
			}

			if (oViewModel.getProperty("/e_ledger")) {
				aInputs.push(oView.byId("inputLedger"));
			}

			if (oViewModel.getProperty("/e_gc")) {
				aInputs.push(oView.byId("inputGC"));
			}

			if (oViewModel.getProperty("/e_ov")) {
				aInputs.push(oView.byId("inputOV"));
				aInputs.push(oView.byId("inputSetor"));
				aInputs.push(oView.byId("inputCanal"));
			}

			if (oViewModel.getProperty("/e_blart")) {
				aInputs.push(oView.byId("inputBlart"));
			}

			var bValidationError = false;

			jQuery.each(aInputs, function(i, oInput) {

				if (oInput.getValue() === "") {
					oInput.setValueState(sap.ui.core.ValueState.Error);
					bValidationError = true;
				} else {
					oInput.setValueState(sap.ui.core.ValueState.None);
				}

			});

			// output result
			if (bValidationError) {
				return;
			}

			sUrl = "/CheckAprovadorSet(Bukrs='" + oViewModel.getProperty("/bukrs") +
				"',Werks='" + oViewModel.getProperty("/werks") +
				"',TpDocWf='" + oViewModel.getProperty("/id_tpdoc_wf") +
				"',CentroCusto='";
				
			if (oViewModel.getProperty("/e_cc")) {
				sUrl += oView.byId("inputCC").getValue();
			}
				
			sUrl += "')";
			
			this.getOwnerComponent().getModel("busyAppView").setProperty("/busy", true);
			oGWModel.setUseBatch(false);
			oGWModel.read(sUrl, // Entity Set
				{
					context: null,
					async: false,
					//filters: aFilter,
					sorters: null,
					success: this._onReadSuccessCheckAprov.bind(this),
					error: this._onReadErrorCheckAprov.bind(this)

				});

		},
		_onReadSuccessCheckAprov: function(oData, oResponse) {
			var oViewModel = this.getView().getModel("displayView");

			if (oData.Flag) {
				this.getOwnerComponent().getModel("global").setProperty("/kostl", oViewModel.getProperty("/kostl"));
				this.getOwnerComponent().getModel("global").setProperty("/ldgrp", oViewModel.getProperty("/ldgrp"));
				this.getOwnerComponent().getModel("global").setProperty("/ekgrp", oViewModel.getProperty("/ekgrp"));
				this.getOwnerComponent().getModel("global").setProperty("/vkorg", oViewModel.getProperty("/vkorg"));
				this.getOwnerComponent().getModel("global").setProperty("/spart", oViewModel.getProperty("/spart"));
				this.getOwnerComponent().getModel("global").setProperty("/vtweg", oViewModel.getProperty("/vtweg"));
				this.getOwnerComponent().getModel("global").setProperty("/blart", oViewModel.getProperty("/blart"));

				this.getRouter().navTo("t_lista", {
					bukrs: oViewModel.getProperty("/bukrs"),
					werks: oViewModel.getProperty("/werks"),
					//tp_negocio: oViewModel.getProperty("/tp_negocio"),
					id_tpdoc_wf: oViewModel.getProperty("/id_tpdoc_wf")
						/*	kostl: oViewModel.getProperty("/kostl"),
							ldgrp: oViewModel.getProperty("/ldgrp"),
							ekgrp: oViewModel.getProperty("/ekgrp"),
							vkorg: oViewModel.getProperty("/vkorg"),
							spart: oViewModel.getProperty("/spart"),
							vtweg: oViewModel.getProperty("/vtweg")*/
				});
			} else {
				MessageBox.error(this.getResourceBundle().getText("main_m01"));
			}

			this.getOwnerComponent().getModel("busyAppView").setProperty("/busy", false);
		},

		_onReadErrorCheckAprov: function(oError) {
			MessageBox.error(this.getResourceBundle().getText("main_m01"));
			this.getOwnerComponent().getModel("busyAppView").setProperty("/busy", false);
		}

	});

});